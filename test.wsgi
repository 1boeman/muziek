import sys
sys.path.insert(0, '/home/joriso/dev/flask_muziek')

activate_this = '/home/joriso/dev/flask_muziek/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

from muziek import create_app

application = create_app()
