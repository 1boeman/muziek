from . import db


def query(sql,arguments=[]):
    cursor = db.get_cursor()
    cursor.execute(sql,arguments)
    r =  cursor.fetchall() 
    cursor.close()
    return r


def get_city_groupings(group_id = None):
    if group_id:
        return query('''select cg.*,ts.* from city_groupings cg 
                        natural left join city_groupings_has_translation_string cghts
                        natural left join translation_string ts
                        where group_id = %s
                        ''',[group_id])
    else:
        return query('''select cg.*,ts.* from city_groupings cg 
                        natural left join city_groupings_has_translation_string cghts
                        natural left join translation_string ts''')


def get_city(city_id):
    return query('''select c.* from city c 
                    where city_id = %s''', ( city_id, ))



def get_streets():
    return query ("select s.*,c.* from  street s natural join city c")



def get_cities():
    return query('select * from city natural join country order by country_id desc ,city_name')


def get_city_venues(city_id):
    q = '''
        select v.*,s.street_name, c.city_id,c.city_name 
            from venue v  
            natural join venue_address va 
            natural join address a 
            natural join street s 

            natural join city c 

            where c.city_id = %s

            order by v.venue_title asc
    '''
    return query(q, ( city_id, ))



def get_venue(venue_id):
    q = '''
        select v.*,s.street_name, 
            c.city_id,c.city_name,
            a.zip, a.number 
            from venue v  
            natural join venue_address va 
            natural join address a 
            natural join street s 
            natural join city c 
            where v.venue_id = %s
    '''
    return query(q, ( venue_id, ))



def get_events(date_start,city=None,city_group=None,limit=100,start=0):
    q = '''
        select * from event e where 1 and (date_start >= %s
        or (date_end is not null and date_end <= %s))
        inner join venue_address va on va.venue_id = e.venue_id and va.address_id = e.address_id
        inner join address a on address_id
    '''

    # pagination
    q +=' limit '+ str(limit) + ' offset ' + str(start)

    
    if city:
        q = 'select * from events'
    elif city_group:
        q = 'select * from events'
    else:
        return query(q,(date_start,date_start))
