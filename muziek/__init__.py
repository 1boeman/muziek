import os
from datetime import datetime
from flask import Flask,g,url_for,abort,request,render_template_string
from flask_babel import Babel, _
from babel.dates import format_datetime, format_time 
from . import index
from . import db
from . import drupal


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        UPLOAD_FOLDER="/tmp",
        ALLOWED_UPLOAD_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'},
        SECURITY_REGISTERABLE=True,
        SECRET_KEY='dev',
        SECURITY_PASSWORD_SALT='1234',
        MYSQL_USER='vagrant',
        MYSQL_PASSWORD='password',
        MYSQL_DB='muziekladder_db',
        BABEL_DEFAULT_LOCALE='nl',
        LANGUAGES=['nl','en'],
        DRUPAL='https://hdm-dev.textarea.nl',
        SOLR='http://localhost:8983/solr',
        SOLRCORE='muziekladder',
        SOLR_API= 'http://localhost:8983/api',
        SOLR_PARAMS_DEFAULT={   
                        "_q0":{'q.op':'AND'},
                        "_q1":{'defType':'edismax'},
                        "_q3":{"rows":100},
                        "_q2":{'qf':'date_start_dt event_title_t event_description_t event_description_txt_nl event_description_txt_en venue_title_s venue_description_t zip_s address_s city_s country_s',},
                        "plaats":{'json.facet':'{plaats:{type:terms,field:city_s,sort:"index asc",limit:20}}'},
                        "land":{'json.facet':'{land:{type:terms,field:country_s,sort:"count desc",limit:20}}'},
                        "groep":{'json.facet':'{provincie:{type:terms,field:city_groupings_id_ss,sort:"count desc",limit:20}}'},
                        "plek":{'json.facet':'{plek:{type:terms,field:venue_title_s,sort:"count desc",limit:20}}'},
                        "land":{'json.facet':'{land:{type:terms,field:country_s,sort:"count desc"}}'},

        },
        
        SOLR_PARAMS_ALT={ 

        },
        SOLR_ALLOWED_USER_PARAMS=['q','fq','start','sort'], # params that are allowed to go directly from flask request args to solr server

    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass


    # BOF babel     
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        return g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])

    app.jinja_env.globals['get_locale'] = get_locale

    @app.url_defaults
    def add_language_code(endpoint, values):
        if 'lang_code' in values or not g.lang_code:
            return
        if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
            values['lang_code'] = g.lang_code



    @app.before_request
    def ensure_lang_support():
        lang_code = g.get('lang_code', None)
        if lang_code and lang_code not in app.config['LANGUAGES']:
            return abort(404)



    @app.url_value_preprocessor
    def get_lang_code(endpoint, values):
        if values is not None:
            g.lang_code = values.pop('lang_code',app.config['BABEL_DEFAULT_LOCALE'])

    # EOF babel  


    from flask_solr_simple import commands
    commands.init_app(app)


    @app.context_processor
    def menu(): 
        other_languages = app.config['LANGUAGES'].copy()
        other_languages.remove(g.lang_code) 
        m = [
            {'url':url_for('agenda.list'), 'text':_('Agenda')},
            {'url':url_for('index.home'), 'text':_('Locaties')},
            {'url':url_for('index.muziekformulier'), 'text':_('Tips')},


        ]

##        for l in other_languages:
#            m.append({'text':l, 'url':'/'+l})

        return dict(menu=m)

 #       
 #           {'text':_('Home'),'url':'/'},
  #      ])
            


    app.register_blueprint(index.bp,name='index_raw')
    app.register_blueprint(index.bp,url_prefix='/<lang_code>/')

    from . import agenda
    app.register_blueprint(agenda.bp,url_prefix='/muziek',name='agenda_raw')
    app.register_blueprint(agenda.bp,url_prefix='/<lang_code>/muziek')

    from . import form
    app.register_blueprint(form.bp,url_prefix='/form',name='form_raw')
    app.register_blueprint(form.bp,url_prefix='/<lang_code>/form')

    from . import search
    app.register_blueprint(search.bp,url_prefix='/search',name='search_raw')
    app.register_blueprint(search.bp,url_prefix='/<lang_code>/search')

    # mysql connection
    db.init_app(app)


    @app.template_filter()
    def format_the_date(value, format='agenda_list'):

        if (get_locale() == 'nl'):
            ll = 'nl_NL'
        else:
            ll = 'en_US'
        if format == 'agenda_list':
            d = datetime.strptime(value,'%Y-%m-%dT00:00:00Z')
            rv = format_datetime(d,'EEE dd MMM',locale=ll)
        elif format == 'agenda_list_header':
            d = datetime.strptime(value,'%Y-%m-%dT00:00:00Z')
            rv = format_datetime(d,'EEEE dd MMMM',locale=ll)
        elif format == 'agenda_header_date':
            rv = format_datetime(value,'EEEE dd MMMM',locale=ll)
        return rv



    @app.before_request
    def set_jinja_globals():
        app.jinja_env.globals['root_url'] = request.root_url


    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello'

    return app


