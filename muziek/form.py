from flask import Blueprint, render_template, request, current_app,redirect
from flask_babel import _
import json
import os
from . import sql
from werkzeug.utils import secure_filename

bp=Blueprint('form', __name__)


@bp.route('/muziekformulier_validate', methods = ['POST'])
def muziekformulier_validate():
    errors = {}
    values = request.form.to_dict() 
    if not len(values['date']) and not values['startDate']:
        errors['date'] = [_('Specificeer aub een datum'),]

    if not len(values['city_select']) or values['city_select'] =="0":
        errors['city_select'] = [_("Selecteer een stad"),]

    if (values['city_select'] == "-1"):
        if not len(values['city_name_manual']):
            errors['city_name_manual'] = [_('De plaatsnaam ontbreekt nog')]
        if not len(values['venue_manual']):
            errors['venue_manual'] = [_('De naam en adres van het podium ontbreken nog')]

    if not len(values["venue_select"]):
        if not len(values['venue_manual']):
            errors['venue_select'] = [_('Selecteer een plaats')]

    if values["venue_select"] == "-1":
         if not len(values['venue_manual']):
            errors['venue_manual'] = [_('De naam en adres van het podium ontbreken nog')]

    if not len(values["title"]):
        errors['title'] = [_('Specificeer een titel voor het evenement')]

    if not len(values["link"]):
        errors['link'] = [_('Specificeer een link naar het evenement of podium')]

    if not errors:
        return None        

    return {"errors":errors}



@bp.route('/get_venue/<venue_id>')
def get_venue_info(venue_id):
    venue = sql.get_venue(venue_id)
    return json.dumps(venue)



@bp.route('/get_city_venues/<city_id>')
def get_city_venues_options(city_id):
    venues = sql.get_city_venues(city_id)
    venue_options = [{"label":_("** Locatie nog niet in deze lijst? Selecteer dan deze optie!"),"value":-1 }]
    for v in venues:
        venue_options.append({
            "label":v['venue_title'],
            "value":v['venue_id']
        })

    venue_options.append({"label":_("** Locatie nog niet in deze lijst? Selecteer dan deze optie!"),"value":-1 })

    return json.dumps(venue_options)



def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_UPLOAD_EXTENSIONS']



@bp.route('/bestand_opladen', methods=['GET', 'POST'])
def upload_file():
    error = "";
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            error = 'No file part'
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            error = "empty filename"
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            #return redirect(url_for('download_file', name=filename))
    return error

