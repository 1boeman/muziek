from flask import (Blueprint,request,render_template,abort)
from flask_solr_simple.client import select_request,select,solr_escape,get_solr
from . import sql
import requests
import json

bp = Blueprint('search', __name__)


@bp.route('/')
def find():
    solr_conf = {}
    solr_parms = []
    data = {}

    q = request.args.get('q')
    data['q'] = q
    solr_parms.append(("q",solr_escape(q)))
    groups = sql.get_city_groupings()
    data['city_groupings'] = dict(map(lambda x: (x['group_id'],x ), groups))


    
    r = select_request(extra_params=solr_parms,config_override=solr_conf)
    data['solr'] = r
    return render_template('search.html',data=data)



@bp.route('/suggest')
def suggest():
    rv = {"suggestions":{}}
    terms = request.args.get('terms')
    if terms:
        solr = get_solr()
        solr = "/".join([solr,'suggest']) 
        params = {
            "suggest":"true",
            "suggest.dictionary":"mySuggester",
            "suggest.q":solr_escape(terms),
        }
        r = requests.get(solr,params)
        r = r.json()
        suggestings = r['suggest']['mySuggester']
        for term in suggestings:
            for y in suggestings[term]['suggestions']:
                rv['suggestions'][y['term']] = {"label":y["term"],"value": y["term"]}
        rv['suggestions'] = list(rv['suggestions'].values())
    return rv


