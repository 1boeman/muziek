(function(conf){
    const pageHandlers ={
        "agenda":function(){
            datePicker(conf);
            const ac = new Autocomplete(document.getElementById('waar-zoek'), {
                data: ML['ac_data'],
                maximumItems: 200,
                dropdownClass:"wide-dropdown",
                onSelectItem: ({label, value}) => {
                    console.log("user selected:", label, value);
                }
            });
        },
        "home":()=>{
            searchBox('zoek-balk');
        },
        "zoeken":()=>{
            searchBox('zoek-balk');
        }
    }; 

    const clickHandlers = {
        "menu-tab":function(){
            location.href = this.querySelector('a').getAttribute('href');
        }
    };
  
    function mkClick(selectorOrEl,callback){
        var listen = function(el,callback){
            el.addEventListener('click',function(e){
                e.preventDefault();
                e.stopPropagation();
                if (typeof callback == 'function'){
                    callback.apply(this);             
                } else if (typeof clickHandlers[this.dataset.handler] == 'function'){
                    clickHandlers[this.dataset.handler].apply(this);
                }
            });
        }
        if (typeof selectorOrEl == 'string'){
            document.querySelectorAll(selectorOrEl).forEach(el => {
               listen(el,callback);
            });
        } else {
            listen(selectorOrEl,callback);
        }
    }


    function searchBox(element_id){
        let t = 0;
        const balk = document.getElementById(element_id);
        const ac = new Autocomplete(balk, {
            data: [],
            maximumItems: 200,
            dropdownClass:"wide-dropdown",
            onInput:function(){
                console.log(balk.value)
                if (t) clearTimeout(t);  
                t = setTimeout(function(){
                    fetch(ML.root_url+'search/suggest?terms=' + encodeURIComponent(balk.value))
                        .then(response => response.json())
                        .then(data => ac.setData(data['suggestions']))
                },200)
            },
            onSelectItem: ({label, value}) => {
                console.log("user selected:", label, value);
            }
        });
    }

    function datePicker({ locale }){
        if (locale == 'nl'){
            var cWD = ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'];
            var cM = ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];
        } else {
             var cWD = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
             var cM = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        }
        const picker = MCDatepicker.create({
            el: '#datepicker',
            customWeekDays:cWD,
            customMonths:cM
        });
    }

    // attach handlers
    document.body.classList.forEach(c => {if (typeof pageHandlers[c] == 'function'){ pageHandlers[c](); }})

    mkClick('.click-me');
})(ML);
