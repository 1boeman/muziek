const LANG = {
    "Datum":{
        "en": "Date"
    },

    "Datumbereik":{
        "en": "Date range"
    },

    "Datum start":{
        "en": "Date start"
    },
    
    "Datum eind":{
        "en": "Date end"
    },
    "Concert of optreden":{
        "en":"Concert or performance"
    },

    "Festival of feest":{
        "en":"Festival or party"
    },
    "Iets anders":{
        "en":"Something else"
    },

    "zin1":{
        "nl":"Wat voor evenement wilt u toevoegen aan de Muziekladder agenda?",
        "en":"What type of event do you want to recommend to the Muziekladder Calendar?"
    },
    "zin2":{
        "nl":"Specificeer de datum of een datum-bereik",
        "en":"Specicy the date or a date range"
    },
    "Locatie":{"en":"Location"}
 
};     

const vertaal = function(str){
    let taal = typeof ML['locale'] == 'string' ? ML['locale'] : 'nl';

    if ( typeof LANG[str] == 'undefined' ) return str;

    return ( typeof LANG[str][taal] == 'string' ? LANG[str][taal] : str );
    
}

export { LANG, vertaal }
