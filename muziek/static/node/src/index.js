import { useState, useEffect } from "react";
import React from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select'
import LocatieSpecs from './components/LocatieSpecs'
import DateTabs from './components/DateTabs'
import InvalidFeedback from './components/InvalidFeedback'
import "./index.css"
import axios from 'axios'
import { vertaal } from "./lang.js";


function MyForm() {
    const [loading,setLoading] = React.useState(false);
    const [fieldErrors,setFieldErrors] = React.useState({});
    const [fieldValues,setFieldValues] = React.useState({
        "eventType":"",
        "whichPicker":"single",
        "date":"",
        "startDate":"",
        "endDate":"",
        "city_select":0,
        "city_name_manual":"",
        "venue_select":"",
        "venue_manual":"",
        "title":"",
        "link":"",
        "selectedFile": null
    });

    useEffect(()=>{
        console.log(fieldValues);
    })
 
    const handleChange = (event, fieldId) => {
        let newFields = { ...fieldValues };
        newFields[fieldId] = event.target.value;
        setFieldValues(newFields);
    };

    const changeFieldErrors = (newFieldErrorsObj) => {
        let newErrors = { ...fieldErrors };
        for (let x in newFieldErrorsObj){
            newErrors[x] = newFieldErrorsObj[x];
        }
        setFieldErrors(newErrors);
    }

    const changeFieldValues = (newFieldValuesObj) => {
        let newFields = { ...fieldValues };
        for (let x in newFieldValuesObj){
            newFields[x] = newFieldValuesObj[x];
        }
        setFieldValues(newFields);
    }

    const uploadFile = () => {
        let formData = new FormData();
        let url  = ML['bestand_target'];
        formData.append("file", fieldValues.selectedFile);
        axios.post(url, formData, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }).then((response) => {
              console.log(response);
        }).catch((error) => {
              console.log(error);
        });
    };



    
    const [name, setName] = useState("");
    const handleSubmit = (event) => {
        setLoading(true);
        event.preventDefault();
        let form_data = new FormData(); 
        for (let key in fieldValues){
            form_data.append(key,fieldValues[key])
        }
        
        axios({
          method: "post",
          url: ML['muziek_form_target'],
          data: form_data,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            //handle success
            console.log(response.data)
            console.log(response.data.errors)
            if (response.data.errors !== undefined){
                
                if (Object.keys(response.data.errors).length){
                    setFieldErrors(response.data.errors);
                    console.log(fieldErrors)
                    setTimeout(function(){setLoading(false)},500);
                } else {
                    uploadFile();
                }
            } else {


            }
             
        })
          .catch(function (response) {
            // @todo handle error
            console.log(response);
        });
        //console.log(name)
    }

    if (loading){
        return (
            <div class="d-flex justify-content-center">
              <div class="spinner-border" role="status">
                <span class="sr-only"></span>
              </div>
            </div>
        );
    }

    return (
        <form onSubmit={handleSubmit}>
            <div class="mb-4">
                <label className="form-label form-label-lg">{vertaal('Soort evenement') }:</label>
                <select onChange = {(e) => changeFieldValues({"eventType" : e.target.value }) } className="form-select form-select-lg">
                    <option value=""> - { vertaal("Selecteren") } - </option>
                    <option value="concert">{ vertaal("Concert of optreden") }</option>
                    <option value="festival">{ vertaal("Festival of feest") }</option>
                    <option value="iets_anders">{ vertaal("Iets anders") }</option>
                 </select>
                <div class="description">{vertaal('zin1')}...</div>
            </div> 
    
        { fieldValues.eventType.length > 0 && 
            <>
                <div className="mb-4 fieldset">
                    <h4>{vertaal('zin2')}<span className="asterisk">*</span></h4>
                    <DateTabs 
                        changeFieldValues={changeFieldValues} 
                        changeFieldErrors={changeFieldErrors} 
                        fieldValues={fieldValues}
                        fieldErrors={fieldErrors}
                    />
                </div>
                <div className="mb-4 fieldset">
                    <LocatieSpecs
                        changeFieldValues={changeFieldValues} 
                        changeFieldErrors={changeFieldErrors} 
                        fieldValues={fieldValues}
                        fieldErrors={fieldErrors}
                    />
                </div>
                <div className="fieldset mb-4">
                    <div className={ fieldErrors.title ? 'invalid-border mb-4' : 'mb-4' }>
                        <label className="form-label">{vertaal('Titel van het evenement / naam van de artiest(en)')}<span className="asterisk">*</span></label>  
                        <input
                          placeholder = {vertaal('Titel/naam')}
                          className="form-control" 
                          type="text" 
                          value={fieldValues.title}
                          onChange={(e) => {
                            changeFieldValues({"title":e.target.value});
                            changeFieldErrors({"title": null});
                          }}
                        />
                        { fieldErrors.title &&
                            <InvalidFeedback 
                                messages={fieldErrors.title} />
                        }
                    </div>
                    <div className={ fieldErrors.link ? 'invalid-border mb-4' : 'mb-4' }>
                        <label className="form-label">{vertaal('Link naar evenement of podium')}<span className="asterisk">*</span></label>  
                        <input
                          placeholder = "https://..."
                          className="form-control" 
                          type="text"
                          value={fieldValues.link}
                          onChange={(e) => {
                            changeFieldValues({"link":e.target.value});
                            changeFieldErrors({"link": null});
                          }}
                        />
                        { fieldErrors.link &&
                            <InvalidFeedback 
                                messages={fieldErrors.link} />
                        }
                    </div>
                </div>
                <div className="fieldset mb-4">
                    <FileUploadField 
                         changeFieldValues={changeFieldValues} 
                         changeFieldErrors={changeFieldErrors} 
                         fieldValues={fieldValues}
                         fieldErrors={fieldErrors}
                     />
                </div>
                <div className="mb-4">
                    <input class="btn btn-primary btn-lg" type="submit" />
                </div>
        </>
        }
        </form>
    )
}

function FileUploadField(props){
    return ( 
        <div className="mb-4">
            <label for="formFileLg" className="form-label">{vertaal('Afbeelding (optioneel) - poster, flyer of logo')}</label>
            <input 
                onChange={(e) => props.changeFieldValues({ "selectedFile": e.target.files[0] })} 
                className="form-control form-control-lg" 
                id="formFileLg" 
                type="file" 
                accept ="image/*"
            />
        </div>
    );
}


ReactDOM.render(<MyForm />, document.getElementById('form_container'));
