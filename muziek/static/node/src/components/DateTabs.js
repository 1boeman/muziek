import { useState } from "react";
import React from 'react';
import 'react-dates/initialize';
import { DateRangePicker, SingleDatePicker } from "react-dates";
import { vertaal } from "../lang.js";
import "react-dates/lib/css/_datepicker.css";
import InvalidFeedback from './InvalidFeedback'

function DateTabs(props){

    const [focusedInput, setFocusedInput] = React.useState();
    const [focused, setFocused] = React.useState();
    const handleDatePickTabClick = (event) =>{
            //setWhichPicker(event.target.dataset.which);
        console.log(props.fieldErrors)
        props.changeFieldValues({"whichPicker":event.target.dataset.which})
    }
    return (
        <div className={ props.fieldErrors.date ? 'invalid-border' : '' }>
            <ul className="nav nav-tabs">
                <li className="nav-item"> 
                    <a
                        className={ props.fieldValues.whichPicker == 'single' ? 'nav-link active' : 'nav-link' } 
                        data-which="single"
                        onClick={ handleDatePickTabClick }> <span className="bullet">&bull;</span> { vertaal('Één dag') }
                    </a>
                </li>
                <li className="nav-item ">
                    <a
                        className={ props.fieldValues.whichPicker == 'range' ? 'nav-link active' : 'nav-link' } 
                        data-which="range" 
                        onClick={ handleDatePickTabClick }> <span className="bullet">&bull;</span> { vertaal('Langer dan één dag') }
                    </a>
                </li>
            </ul>
            <div className="tab-content bnbDatePicker"> 
                <div className={ props.fieldValues.whichPicker == 'single' ? 'tab-pane show active fade' : 'tab-pane' }>
                { props.fieldValues.whichPicker == 'single' &&
                  <SingleDatePicker
                    placeholder={ vertaal('Datum')}
                    daySize={50}
                    date={props.fieldValues.date}
                    showDefaultInputIcon
                    onDateChange={(date) => {
                        props.changeFieldValues({"date":date}); 
                        props.changeFieldErrors({"date":null});
                    }}
                    focused={focused}
                    onFocusChange={({ focused }) => setFocused(focused)}
                    id="date"
                  />
                }
                </div>
                <div className={ props.fieldValues.whichPicker == 'range' ? 'tab-pane show active fade' : 'tab-pane' }>
                { props.fieldValues.whichPicker == 'range' &&
                  <DateRangePicker
                    startDatePlaceholderText= { vertaal("Datum start") }
                    endDatePlaceholderText={ vertaal("Datum eind") }
                    startDate={props.fieldValues.startDate}
                    startDateId="start-date"
                    daySize={50}
                    showDefaultInputIcon
                    endDate={props.fieldValues.endDate}
                    endDateId="end-date"
                    onDatesChange={({ startDate, endDate }) => {
                        props.changeFieldValues({'startDate':startDate,'endDate':endDate});
                        props.changeFieldErrors({"date":null});
                    }}
                    focusedInput={focusedInput}
                    onFocusChange={(focusedInput) => setFocusedInput(focusedInput)}
                  />
                }
                </div>
            </div>
            { props.fieldErrors.date &&
                <InvalidFeedback 
                    messages= {props.fieldErrors.date}
                />
            }
        </div>);
}

export default DateTabs;
