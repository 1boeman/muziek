import React from 'react';

function InvalidFeedback(props){

    return (
        <div className="invalid-feedback">
        { props.messages.map((txt) =>  
            <div><strong>{txt}</strong></div>
        )}
        </div>
    );
}

export default InvalidFeedback;
