import { useState, useEffect } from "react";
import React from 'react';
import { vertaal } from "../lang.js";
import Select from 'react-select'
import axios from 'axios'
import InvalidFeedback from './InvalidFeedback'

function LocatieSpecs(props){

    const [cityVenuesOptions, setCityVenuesOptions] = useState([]);
    const [displayVenue,setDisplayVenue] = useState({});


    React.useEffect(()=>{
        setCityVenuesOptions([])
        if (props.fieldValues.city_select){
            axios.get(ML.root_url + 'form/get_city_venues/'+props.fieldValues.city_select)
                .then(function(response){
                    setCityVenuesOptions(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                }) 
        }
    },[props.fieldValues.city_select])

    React.useEffect(()=>{
        if (props.fieldValues.venue_select && props.fieldValues.venue_select != '-1'){
            axios.get(ML.root_url + 'form/get_venue/'+props.fieldValues.venue_select)
                .then(function(response){
                    if(response.data.length){
                        setDisplayVenue(response.data[0]);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                }) 
        }
    },[props.fieldValues.venue_select])



    return (
        <>
           <div className={ props.fieldErrors.city_select ? 'mb-4 invalid-border' : 'mb-4' }>
                <h4>{vertaal('Plaats')}*</h4>
                <Select
                    className='react-select-container'
                    classNamePrefix="react-select"
                    options={ML['city_options']} 
                    isSearchable = { true }
                    isClearable = { true }
                    isDisabled = {Boolean(props.fieldValues.city_name_manual.length)}
                    onChange={ (selected) => {
                        props.changeFieldValues({"city_select": selected.value,"venue_select":"" });
                        props.changeFieldErrors({"city_select": null,"venue_select":null });
                        setDisplayVenue({});
                    }}
                />


                { props.fieldErrors.city_select &&
                    <InvalidFeedback 
                        messages={props.fieldErrors.city_select}/>
                }
            </div>
            { props.fieldValues.city_select == '-1' &&
            <div className={ props.fieldErrors.city_name_manual ? 'mb-4 invalid-border' : 'mb-4' }>
                <h4>{vertaal('Naam van de stad, gemeente of het dorp waarin het evenement plaats heeft') }.*</h4>
                <div class="mb-3">
                    <input 
                        type="text"
                        className="form-control" 
                        placeholder={vertaal('Plaatsnaam')}
                        value={props.fieldValues.city_name_manual}
                        onChange={(e) => {
                            props.changeFieldValues({ 'city_name_manual' : e.target.value,"venue_select":"" });
                            props.changeFieldErrors({"city_manual": null,"venue_select":null });
                            setDisplayVenue({});
                        }}
                />
                </div>
              { props.fieldErrors.city_name_manual &&
                    <InvalidFeedback 
                        messages={props.fieldErrors.city_name_manual }
                    />
              }

            </div>
            }
            { props.fieldValues.city_select > 0 &&
                cityVenuesOptions.length > 2 &&
                    <div className={ props.fieldErrors.venue_select ? 'mb-4 invalid-border' : 'mb-4' }>
                        <h4>{vertaal('Kies een locatie uit lijst')}*</h4>
                        <Select
                            className='react-select-container'
                            classNamePrefix="react-select"
                            options={cityVenuesOptions}
                            isSearchable = { true }
                            isClearable = { true }
                            onChange={ (selected) => {
                                props.changeFieldValues({"venue_select": selected.value});
                                props.changeFieldErrors({"venue_select":null });
                                setDisplayVenue({});
                            }}
                        />
                        { props.fieldErrors.venue_select &&
                            <InvalidFeedback 
                                messages={props.fieldErrors.venue_select}
                            />
                        }
                            {displayVenue.venue_title &&
                                <div className="displayVenue shadow-sm p-2 mb-4 mt-4 bg-secondary rounded ">
                                    <div>&bull; <strong> { displayVenue.venue_title }</strong></div>
                                    <div>&bull; { displayVenue.hasOwnProperty("street_name") ? displayVenue.street_name : ""}&nbsp;
                                         { displayVenue.hasOwnProperty("number") ? displayVenue.number : ""}&nbsp;
                                         { displayVenue.city_name }</div>
                                </div>
                            }
                    </div>
            }
            { ( props.fieldValues.venue_select == '-1' ||  
                props.fieldValues.city_select == '-1' || 
                (props.fieldValues.city_select > 0 && 
                 cityVenuesOptions.length == 2)
              )  &&
                <div className= { props.fieldErrors.venue_manual ? 'mb-4 invalid-border' : 'mb-4' }>
                    <label htmlFor="textarea2">{vertaal('Naam en adres van het podium, cafe, terrein of club waar het evenement plaats zal vinden.')}*</label>
                    <textarea 
                        className="form-control" 
                        id="textarea2" 
                        placeholder={vertaal('Naam / adres')}

                        onChange={ (e) => {
                                props.changeFieldValues({"venue_manual": e.target.value});
                                props.changeFieldErrors({"venue_manual":null, "venue_select":null });
                        }}
                     />
                     { props.fieldErrors.venue_manual &&
                        <InvalidFeedback 
                            messages={props.fieldErrors.venue_manual }
                        />
                     }
                </div>
            }
        </>
    );
}

export default LocatieSpecs;
