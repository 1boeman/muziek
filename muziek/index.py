from flask import Blueprint, render_template, request
from flask_babel import _
import json
from . import sql
from . import drupal

bp=Blueprint('index', __name__)



@bp.route('/')
def home():
    data = {}
    data["city_groupings"] = sql.get_city_groupings()
    data["articles"] = drupal.frontpage()
    return data["articles"]
    return render_template('home.html',data=data)



@bp.route('/muziekformulier')
def muziekformulier():
    data = {
        "city_options" : [{"label":_("** Plaatsnaam nog niet in deze lijst? Selecteer dan deze optie!"),"value":-1}]
    }
    cities = sql.get_cities()
    for c in cities:
        data["city_options"].append( {
                "label":c['city_name'],
                "value":c['city_id']
        }) 

    data["city_options"].append( {
            "label":_("** Plaatsnaam nog niet in deze lijst? Selecteer dan deze optie!"),
            "value":-1
    }) 
    
    data['city_options'] = json.dumps(data['city_options'])
    
    return render_template('muziek_form.html',data=data)


