import json
import requests
from flask import (current_app, g, request)


def frontpage():
    drup = get_drupal()
    url = "/".join([drup,"api/v1/frontpage?_format=hal_json"]) 
    r = requests.get(url)  
    return r.json()



def get_drupal():
    current_app
    if 'drupal' not in g:
        g.drupal = current_app.config['DRUPAL']
    return g.drupal



