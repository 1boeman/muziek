from flask import Blueprint,render_template,g,abort
from . import sql
from datetime import datetime,timedelta
from flask_solr_simple.client import select_request,select,solr_escape


bp=Blueprint('agenda', __name__)


@bp.route('/')
@bp.route('/<location>/')
@bp.route('/<location>/<day>')
def list(location=None, day=None):
    data = {'location':''} 
    solr_conf = {}
    solr_parms = []
    q = "*:*"
    solr_parms.append(("sort",'date_start_dt asc,city_s asc'))

    #date  filter 
    now = datetime.now()
    now = now - timedelta(hours = 4) # don't switch to next day till 4 am
    date_start = now.strftime('%Y-%m-%d')
    data['date_start'] = date_start
    data['now'] = now
    data['location_name'] = ''
    solr_parms.append(("fq",'date_start_dt:[' + date_start + 'T00:00:00Z TO *]'))
    if location:
        # regio
        if len(location) > 5 and location[0:6] == 'regio-':
            group_id = location[6:] 
            group_data = sql.get_city_groupings(group_id)
            data['group_data'] = group_data[0]
            if g.lang_code != 'nl' and data['group_data']['string']:
                data['location_name'] = data['group_data']['string']
            else:
                data['location_name'] = data['group_data']['group_name']
            solr_parms.append(("fq",'city_groupings_id_ss:' + group_id))
        else:
            # stad
            city_list = location.split("-")
            city_id = str(city_list[0])
            if city_id.isnumeric():
                city_data = sql.get_city(city_id)
                data['location_name'] = city_data[0]['city_name']
                solr_parms.append(("fq","city_i:"+ city_id))
            else:
                abort(404)
    if day:
        pass
         

    solr_parms.append(("q",q))
    r = select_request(extra_params=solr_parms,config_override=solr_conf)
    data['solr'] = r
    cities = sql.get_cities()
    streets = sql.get_streets()
    groups = sql.get_city_groupings()

    data['city_groupings'] = dict(map(lambda x: (x['group_id'],x ), groups))
    
    data['location_autocomplete'] = []
    for x in cities:
        v = x['city_name']
        data['location_autocomplete'].append({"label":v,"value":v})
    for x in groups:
        v = x['group_name']
        data['location_autocomplete'].append({"label":v,"value":v})
    for x in streets:
        v = x["street_name"] + ', ' + x['city_name']
        data['location_autocomplete'].append({"label":v,"value":v})

    return render_template('agenda.html',data=data)
